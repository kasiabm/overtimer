# OVERTIMER

A Ruby on Rails application based on the online course *Professional Rails Code Along* by **Jordan Hudgens**.

Originally developed to record overtime hours the application was expanded to record daily hours worked by employees.

It utilises a range of gems to provide out of the box functionality such as:
1. Administrate - dashboard for administrators
2. Devise - for logging in users
3. Gritter - for in-app notifications
4. Pundit - for authorisation
5. Twilio-ruby - for messaging

just to name the few.

It also uses custom rake tasks for notifications.

The application was developed using TDD / BDD with Rspec and Capybara.