require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    @user = FactoryBot.create(:user)
  end

  describe "create" do
    it "should create valid user" do	  
      expect(@user).to be_valid
    end
  end

  describe "validate" do
    it "should not create user without first name" do
      @user.first_name = nil
      expect(@user).to_not be_valid
    end

    it "should not create user without last name" do
      @user.last_name = nil
      expect(@user).to_not be_valid
    end

    it "should not create user without phone number" do
      @user.phone_number = nil
      expect(@user).to_not be_valid
    end

    it "should not create user without national insurance number" do
      @user.ni_number = nil
      expect(@user).to_not be_valid
    end

    it "national insurance number should contain 2 letters, 6 digits and 1 letter" do
      @user.ni_number = "1234BA"
      expect(@user).to_not be_valid
    end
	
    it "should not create user without company" do
      @user.company = nil
      expect(@user).to_not be_valid
    end

    it "phone number should only contain integers" do
      @user.phone_number = "greatstring"
      expect(@user).to_not be_valid
    end

    it "phone number should contain only 11 characters" do
      @user.phone_number = "0123456789"
      expect(@user).to_not be_valid
   end
  end 

  describe "custom name method" do
    it "should combine first and last name" do
      expect(@user.full_name).to eq("DONCADILL, STEVE")
    end
  end

  describe "relationship between admins and employees" do
    it "should allow admins to be associated with multiple employees" do
      employee_1 = FactoryBot.create(:user)
      employee_2 = FactoryBot.create(:user)
      admin = FactoryBot.create(:admin_user)
      Hand.create!(user_id: admin.id, hand_id: employee_1.id)
	  Hand.create!(user_id: admin.id, hand_id: employee_2.id)
	  expect(admin.hands.count).to eq(2)
    end
  end  
end
