require 'rails_helper'

RSpec.describe Post, type: :model do
  describe "Post" do
    before do
      @user = FactoryBot.create(:user)
      @post = FactoryBot.create(:post)
    end

    it "should create valid post" do
      expect(@post).to be_valid
    end

    it "should not create post without date" do
      @post.date = nil
      expect(@post).to_not be_valid
    end

    it "should not create post without work performed" do
      @post.work_performed = nil
      expect(@post).to_not be_valid
    end

    it "should not create post without daily hours" do
      @post.daily_hours = nil
      expect(@post).to_not be_valid
    end

    it "should have daily hours with value greater than 0.0" do
      @post.daily_hours = 0.0
	  expect(@post).to_not be_valid
    end
  end
end
