require 'rails_helper'

describe "Post" do
  let(:user) {FactoryBot.create(:user)}
  let(:post) do
    Post.create(date: Date.today, 
                work_performed: "Work performed", 
                user_id: user.id,
                daily_hours: 9.5)
  end
  
  before do
    login_as(user, :scope => :user)
    visit new_post_path
  end

  describe "index" do
    before do
      visit posts_path
    end

    it "should be reached successfully" do
      expect(page.status_code).to eq(200)
    end

    it "should have Posts title" do      
      expect(page).to have_content(/Posts/)
    end

    it "should have a list of posts" do
      post1 = FactoryBot.create(:post)
      post2 = FactoryBot.create(:second_post)
      post1.update!(user_id: user.id)
      post2.update!(user_id: user.id)
      visit posts_path
      expect(page).to have_text(post1.work_performed)
      expect(page).to have_text(post2.work_performed)
    end
	
    it "should display only posts belonging to the creator" do
	  another_user = User.create(first_name: "Bob", 
                                 last_name: "Uncles",
                                 email: "bob@bob.com",
                                 password: "bobster14",
                                 password_confirmation: "bobster14",
                                 phone_number: "01296827034")
      another_user_post = Post.create(date: Date.today, 
                                      work_performed: "This post should not be displayed", 
                                      user_id: another_user.id,
                                      daily_hours: 9.5)
      visit posts_path
      expect(page).to_not have_content(/This post should not be displayed/)
    end
  end

  describe "new" do
    it "should have a link from homepage" do
      employee = Employee.create(first_name: "Bob", 
                                 last_name: "Uncles",
                                 email: "bob@bob.com",
                                 password: "bobster14",
                                 password_confirmation: "bobster14",
                                 phone_number: "01296827034")
      login_as(employee, :scope => :user)
      visit root_path
      click_link "new_post_from_nav"
    end
  end

  describe "create" do  
    before do
      visit new_post_path
    end

    it "has new form that should be reached" do
      expect(page.status_code).to eq(200)
    end

    it "should be created from new form page" do
      fill_in "post[date]", with: Date.today
      fill_in "post[work_performed]", with: "some work performed"
	  fill_in "post[daily_hours]", with: 9.5
      expect {click_on "Save"}.to change(Post, :count).by(1)
    end

    it "should have a user associated with post" do
      fill_in "post[date]", with: Date.today
      fill_in "post[work_performed]", with: "some work performed"
	  fill_in "post[daily_hours]", with: 7.5
      click_on "Save"
      expect(User.last.posts.last.work_performed).to eq("some work performed")
    end
  end

  describe "edit" do
    it "should be editable" do
      visit edit_post_path(post)
      fill_in "post[date]", with: Date.today
      fill_in "post[work_performed]", with: "Edited content"
      click_on "Save"
      expect(User.last.posts.last.work_performed).to eq("Edited content")
    end
	
    it "should not be edited by unauthorised user" do
      logout(:user)
      unauthorised_user = FactoryBot.create(:unauthorised_user)
      login_as(unauthorised_user, :scope => :user)
      visit edit_post_path(post)
      expect(current_path).to eq(root_path)
    end
  end

  describe "delete" do
    it "should be deleted" do
      logout(:user)
      delete_user = FactoryBot.create(:user)
      login_as(delete_user, :scope => :user)
      post_to_delete = Post.create(date: Date.today, 
                                   work_performed: "Stuff", 
                                   user_id: delete_user.id,
                                   daily_hours: 3.5)
      visit posts_path
      click_link "delete_post_#{post_to_delete.id}_from_index"
      expect(page.status_code).to eq(200)
    end
  end
end