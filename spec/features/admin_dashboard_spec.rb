require 'rails_helper'

describe "Admin dashboard" do
  it "should not allow access not logged in users" do
    visit admin_root_path
    expect(current_path).to eq(new_user_session_path)
  end

  it "should not be reached by non-admin users" do
    user = FactoryBot.create(:user)
    login_as(user, :scope => :user)
    visit admin_root_path
    expect(current_path).to eq(root_path)
  end

  it "should be reached by admin users" do
    admin_user = FactoryBot.create(:admin_user)
    login_as(admin_user, :scope => :user)
    visit admin_root_path
    expect(current_path).to eq(admin_root_path)
  end
end
