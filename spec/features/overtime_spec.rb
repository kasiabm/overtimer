require 'rails_helper'

describe "overtime calculations" do
  it "should calculate accurate overtime values for salaried employees" do
    thousand_43 = SalariedOvertimeCalculator.calculate weekly_salary: 1000.0, total_hours: 43.0
    nine_hundred_55 = SalariedOvertimeCalculator.calculate weekly_salary: 900.0, total_hours: 55.5
    expect(thousand_43.to_i).to eq(34)
    expect(nine_hundred_55.to_i).to eq(125)
  end
  
  it "should return 0.0 for an overtime amount if the total hours worked are 40 or less" do
    fourty_or_less = SalariedOvertimeCalculator.calculate weekly_salary: 900.0, total_hours: 40.0
    expect(fourty_or_less.to_i).to eq(0)
  end
end
