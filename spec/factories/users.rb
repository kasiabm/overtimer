FactoryBot.define do
  sequence :email do |n|
    "text#{n}@example.com"
  end

  factory :user do
    email { generate :email }
    password "password"
    password_confirmation "password"
    first_name "Steve"
    last_name "Doncadill"
    phone_number "01296827034"
    ni_number "AB123456C"
    company "Dodgy Business"
  end
  
  factory :unauthorised_user, class: "User" do
    email { generate :email }
    password "password"
    password_confirmation "password"
    first_name "Andy"
    last_name "Doncadill"
    phone_number "01296827034"
    ni_number "AB123456C"
    company "Dodgy Business"
  end

  factory :admin_user, class: "AdminUser" do
    email { generate :email }
    password "password"
    password_confirmation "password"
    first_name "Phil"
    last_name "Bonneville"
    phone_number "01296827034"
    ni_number "AB123456C"
    company "Dodgy Business"
  end
end
