FactoryBot.define do
  factory :post do
    date Date.today
    work_performed "anything"
    daily_hours 13.5
    user
  end

  factory :second_post, class: "Post" do
    date Date.yesterday
    work_performed "second anything"
    daily_hours 8.0
    user
  end
end