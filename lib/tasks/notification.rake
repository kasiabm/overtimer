namespace :notification do
  desc "Sends SMS notification to employees asking them to log any overtime"
  task sms: :environment do
    if not_weekend
      employees = Employee.all
      notification_message = "Please log into the time tracking dashboard to submit your hours "
                           + "for yesterday."
      employees.each do |employee|
        AuditLog.create!(user_id: employee.id)
        if Employee.submitted_yesterday(employee).empty?
          SmsTool.send_sms(message: notification_message, number: employee.phone_number)
        end
      end
    end
  end
  
  desc "Sends mail notification to managers (admin users) each day to inform of pending overtime requests"
  task manager_email: :environment do
    submitted_posts = Post.submitted
    admin_users = AdminUser.all
    if submitted_posts.count > 0
      admin_users.each do |admin|
        ManagerMailer.email(admin).deliver_later
      end
    end 
  end
  
  private
  def not_weekend
    !Time.now.saturday? || !Time.now.sunday?
  end
end
