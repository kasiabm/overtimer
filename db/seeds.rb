@employee = Employee.create!(first_name: "Steve", 
                     last_name: "Beagle", 
                     email: "steve@steve.com", 
                     password: "steve13", 
                     password_confirmation: "steve13",
                     phone_number: "01296827034",
                     ni_number: "AB123456C",
                     company: "Dodgy Business")
puts "1 employee created"

AdminUser.create!(first_name: "Steve", 
                  last_name: "Anders", 
                  email: "steve@admin.com", 
                  password: "admin13", 
                  password_confirmation: "admin13",
                  phone_number: "01296827034",
                  ni_number: "EF123456G",
                  company: "Dodgy Business")
puts "1 admin user created"
			 
100.times do |post|
  Post.create(date: Date.today, 
              work_performed: "#{post} Aenean ultricies vulputate odio hendrerit sodales. Nunc a condimentum mi.", 
              user_id: @employee.id,
              daily_hours: 12.5)
end
puts "100 posts were created"

AuditLog.create(user_id: @employee.id,
                status: 0,
                start_date: Date.today - 6.days)
AuditLog.create(user_id: @employee.id,
                status: 0,
                start_date: Date.today - 13.days)
AuditLog.create(user_id: @employee.id,
                status: 0,
                start_date: Date.today - 20.days)
puts "3 audit logs created"