class User < ApplicationRecord
  has_many :posts
  has_many :audit_logs
  has_many :hands_associations, class_name: "Hand"
  has_many :hands, through: :hands_associations
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates_presence_of :first_name, :last_name, :phone_number, :ni_number, :company
  
  PHONE_NUMBER_REGEX = /\A[0-9]*\Z/
  NI_NUMBER_REGEX = /\A[A-Z]{2}[0-9]{6}[A-Z]{1}\Z/
  
  validates_format_of :phone_number, with: PHONE_NUMBER_REGEX
  validates :phone_number, length: {is: 11}
  validates_format_of :ni_number, with: NI_NUMBER_REGEX
  
  def full_name
    last_name.upcase + ", " + first_name.upcase
  end
end
